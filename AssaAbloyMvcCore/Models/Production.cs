﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AssaAbloyMvcCore.Models
{
    public class Production
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductionId { get; set; }
        public int Aantal { get; set; }
        [DataType(DataType.Date)]
        public DateTime Creationdate { get; set; }
        public string Technisch { get; set; }
        public bool? Done { get; set; }
        public int NumbersId { get; set; }
        public Numbers Numbers { get; set; }
    }
}
