﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AssaAbloyMvcCore.Models
{
    public class ConnectionStringClass : DbContext 
    {
        public ConnectionStringClass(DbContextOptions<ConnectionStringClass> options):base(options)
        {

        }
        public DbSet<Production> Production { get; set; }
        public DbSet<Numbers> Numbers { get; set; }
    }
}
