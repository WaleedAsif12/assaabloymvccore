﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AssaAbloyMvcCore.Models
{
    public class Numbers
    {
        [Key]
        public int NumbersId { get; set; }
        public string Referentie { get; set; }
        public string Code { get; set; }
        public string Technisch { get; set; }
    }
}
