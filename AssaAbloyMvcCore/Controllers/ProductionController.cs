﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AssaAbloyMvcCore.Models;
using Microsoft.EntityFrameworkCore;

namespace AssaAbloyMvcCore.Controllers
{
    public class ProductionController : Controller
    {
        private readonly ConnectionStringClass _cc;
        public ProductionController(ConnectionStringClass cc)
        {
            _cc = cc;
        }
        public IActionResult Index()
        {
            var results = _cc.Production.ToList();
            return View(results);
        }
        public static string Reverse(string str)
        {
            //char[] charStr = str.ToCharArray();
            //int size = charStr.Length;
            //Console.WriteLine(str);

            //var end = size - 1;
            //for (int i = 0; i <= end; i++)
            //{
            //    char temp = charStr[i];
            //    charStr[i] = charStr[end];
            //    charStr[end] = temp;
            //    end--;
            //}
            //string reverseStr = String.Join<char>(",", charStr);
            //return reverseStr;

            char[] charStr = str.ToCharArray();
            Array.Reverse(charStr);
            string reverseStr = String.Join<char>(",", charStr);
            return reverseStr;
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Production production)
        {

            var number = _cc.Numbers.Where(n => n.NumbersId == production.NumbersId).FirstOrDefault();
            var technisch = number.Technisch;
            string reverseTechnisch = Reverse(technisch);

            production.Technisch=reverseTechnisch;
            _cc.Add(production);
            _cc.SaveChanges();
            ViewBag.message = "New Record is saved successfully....! Production Id is " + production.ProductionId + " with technish "+production.Technisch;
            return View(production);
        }
        
    }
}